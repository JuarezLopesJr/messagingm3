package com.example.messagingm3

import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.longClick
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performTouchInput
import com.example.messagingm3.ui.screens.MessageItem
import com.example.messagingm3.utils.Tag.TAG_IMAGE
import com.example.messagingm3.utils.Tag.TAG_TEXT
import com.example.messagingm3.utils.TestDataFactory
import com.example.messagingm3.utils.TestDataFactory.randomString
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class MessageTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Text_Content_Displayed() {
        val message = randomString()

        composeTestRule.setContent {
            MessageItem(
                message = TestDataFactory.makeMessage(text = message),
                onLongPress = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_TEXT).assertTextEquals(message)
    }

    @Test
    fun assert_Text_Body_Never_Displayed() {
        composeTestRule.setContent {
            MessageItem(
                message = TestDataFactory.makeMessage(
                    image = R.drawable.ic_launcher_foreground
                ),
                onLongPress = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_TEXT).assertDoesNotExist()
    }

    @Test
    fun assert_Image_Body_Displayed() {
        composeTestRule.setContent {
            MessageItem(
                message = TestDataFactory.makeMessage(
                    image = R.drawable.ic_launcher_foreground
                ),
                onLongPress = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_IMAGE).assertIsDisplayed()
    }

    @Test
    fun assert_Image_Body_Never_Displayed() {
        composeTestRule.setContent {
            MessageItem(
                message = TestDataFactory.makeMessage(
                    text = randomString()
                ),
                onLongPress = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_IMAGE).assertDoesNotExist()
    }

    @Test
    fun assert_Long_Press_Triggered() {
        val parentTag = "parent_tag"
        val onLongPress: (String) -> Unit = mock()
        val message = TestDataFactory.makeMessage(text = randomString())

        composeTestRule.setContent {
            MessageItem(
                modifier = Modifier.testTag(parentTag),
                message = message,
                onLongPress = onLongPress
            )
        }

        composeTestRule.onNodeWithTag(parentTag).performTouchInput {
            longClick()
        }

        verify(onLongPress).invoke(message.id)
    }
}