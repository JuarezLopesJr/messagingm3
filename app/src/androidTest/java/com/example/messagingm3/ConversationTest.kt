package com.example.messagingm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import com.example.messagingm3.data.ConversationState
import com.example.messagingm3.ui.screens.Conversation
import com.example.messagingm3.utils.Tag.TAG_HEADER
import com.example.messagingm3.utils.Tag.TAG_INPUT_BAR
import com.example.messagingm3.utils.Tag.TAG_MESSAGES
import com.example.messagingm3.utils.Tag.TAG_MESSAGE_ACTIONS
import com.example.messagingm3.utils.TestDataFactory.makeMessage
import org.junit.Rule
import org.junit.Test

class ConversationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Header_Displayed() {
        composeTestRule.setContent {
            Conversation(state = ConversationState(), handleEvent = {})
        }

        composeTestRule.onNodeWithTag(TAG_HEADER).assertIsDisplayed()
    }

    @Test
    fun assert_Messages_Displayed() {
        composeTestRule.setContent {
            Conversation(state = ConversationState(), handleEvent = {})
        }

        composeTestRule.onNodeWithTag(TAG_MESSAGES).assertIsDisplayed()
    }

    @Test
    fun assert_Input_Bar_Displayed() {
        composeTestRule.setContent {
            Conversation(state = ConversationState(), handleEvent = {})
        }

        composeTestRule.onNodeWithTag(TAG_INPUT_BAR).assertIsDisplayed()
    }

    @Test
    fun assert_Messages_Actions_Displayed() {
        composeTestRule.setContent {
            Conversation(
                state = ConversationState(
                    selectedMessage = makeMessage(text = "test message")
                ),
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_MESSAGE_ACTIONS).assertIsDisplayed()
    }

    @Test
    fun assert_Messages_Actions_Not_Displayed() {
        composeTestRule.setContent {
            Conversation(
                state = ConversationState(),
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_MESSAGE_ACTIONS).assertDoesNotExist()
    }
}