package com.example.messagingm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.messagingm3.ui.screens.Header
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class HeaderTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Title_Displayed() {
        composeTestRule.setContent {
            Header(onClose = {})
        }

        composeTestRule.onNodeWithText(getTestString(R.string.title_chat)).assertIsDisplayed()
    }

    @Test
    fun assert_Close_Listener_Triggered() {
        val onClose: () -> Unit = mock()

        composeTestRule.setContent {
            Header(onClose = onClose)
        }

        composeTestRule
            .onNodeWithContentDescription(getTestString(R.string.cd_close_conversation))
            .performClick()

        verify(onClose).invoke()
    }
}