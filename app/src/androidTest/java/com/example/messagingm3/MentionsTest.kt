package com.example.messagingm3

import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.example.messagingm3.ui.screens.Mentions
import com.example.messagingm3.utils.ContactFactory
import com.example.messagingm3.utils.Tag.TAG_CONTACTS
import com.example.messagingm3.utils.stripMentionSymbol
import java.util.Locale
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class MentionsTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Contacts_Displayed() {
        val contacts = ContactFactory.makeContacts()

        composeTestRule.setContent {
            Mentions(
                contacts = contacts,
                query = "@",
                onMentionClicked = { _, _ -> }
            )
        }

        contacts.forEachIndexed { index, contact ->
            composeTestRule.onNodeWithTag(TAG_CONTACTS)
                .onChildAt(index)
                .assertTextEquals(contact.name)
        }
    }

    @Test
    fun assert_Filtered_Contacts_Displayed() {
        val contacts = ContactFactory.makeContacts()
        val query = "@ch"
        val mentions = contacts.filter {
            val withoutMentionSymbol =
                stripMentionSymbol(
                    query.lowercase(Locale.getDefault())
                )

            it.name.lowercase(Locale.getDefault()).startsWith(withoutMentionSymbol)
        }

        composeTestRule.setContent {
            Mentions(
                contacts = contacts,
                query = query,
                onMentionClicked = { _, _ -> }
            )
        }

        mentions.forEachIndexed { index, contact ->
            composeTestRule.onNodeWithTag(TAG_CONTACTS)
                .onChildAt(index)
                .assertTextEquals(contact.name)
        }
    }

    @Test
    fun assert_Contact_Triggers_Callback() {
        val onMentionClicked:(String, String) -> Unit = mock()
        val contacts = ContactFactory.makeContacts()
        val query = "@"

        composeTestRule.setContent {
            Mentions(
                contacts = contacts,
                query = query,
                onMentionClicked = onMentionClicked
            )
        }

        composeTestRule.onNodeWithTag(TAG_CONTACTS)
            .onChildAt(1)
            .performClick()

        verify(onMentionClicked).invoke(query, "@" + contacts[1].name)
    }
}