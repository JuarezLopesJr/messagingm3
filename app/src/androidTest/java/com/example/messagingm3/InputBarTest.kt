package com.example.messagingm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsEnabled
import androidx.compose.ui.test.assertIsNotEnabled
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import com.example.messagingm3.ui.screens.InputBar
import com.example.messagingm3.utils.ContactFactory
import com.example.messagingm3.utils.Tag.TAG_MENTIONS
import com.example.messagingm3.utils.Tag.TAG_MESSAGE_INPUT
import com.example.messagingm3.utils.Tag.TAG_SEND_MESSAGE
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class InputBarTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Send_Message_Enabled_With_Text() {
        composeTestRule.setContent {
            InputBar(sendMessage = {}, contacts = emptyList())
        }

        composeTestRule.onNodeWithTag(TAG_MESSAGE_INPUT)
            .performTextInput("Cacildis")

        composeTestRule.onNodeWithTag(TAG_SEND_MESSAGE).assertIsEnabled()
    }

    @Test
    fun assert_Send_Message_Disabled_With_No_Text() {
        composeTestRule.setContent {
            InputBar(sendMessage = {}, contacts = emptyList())
        }

        composeTestRule.onNodeWithTag(TAG_SEND_MESSAGE).assertIsNotEnabled()
    }

    @Test
    fun assert_Send_Listener_Triggered() {
        val sendMessage: (String) -> Unit = mock()
        val message = "Cacildis"

        composeTestRule.setContent {
            InputBar(sendMessage = sendMessage, contacts = emptyList())
        }

        composeTestRule.onNodeWithTag(TAG_MESSAGE_INPUT)
            .performTextInput(message)

        composeTestRule.onNodeWithTag(TAG_SEND_MESSAGE).performClick()

        verify(sendMessage).invoke(message)
    }

    @Test
    fun assert_Contacts_Not_Displayed_By_Default() {
        composeTestRule.setContent {
            InputBar(sendMessage = {}, contacts = ContactFactory.makeContacts())
        }

        composeTestRule.onNodeWithTag(TAG_MENTIONS).assertDoesNotExist()
    }

    @Test
    fun assert_Contacts_Displayed_When_Mention_Typed() {
        composeTestRule.setContent {
            InputBar(sendMessage = {}, contacts = ContactFactory.makeContacts())
        }

        composeTestRule.onNodeWithTag(TAG_MESSAGE_INPUT)
            .performTextInput("@")

        composeTestRule.onNodeWithTag(TAG_MENTIONS).assertIsDisplayed()
    }

    @Test
    fun assert_Contacts_Hidden_When_Continuing_Message() {
        composeTestRule.setContent {
            InputBar(sendMessage = {}, contacts = ContactFactory.makeContacts())
        }

        composeTestRule.onNodeWithTag(TAG_MESSAGE_INPUT)
            .performTextInput("@Chaves del ocho")

        composeTestRule.onNodeWithTag(TAG_MENTIONS).assertDoesNotExist()
    }
}