package com.example.messagingm3

import androidx.compose.ui.test.assertAny
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onNodeWithTag
import com.example.messagingm3.ui.screens.Messages
import com.example.messagingm3.utils.MessageFactory
import com.example.messagingm3.utils.Tag.TAG_EMPTY
import com.example.messagingm3.utils.Tag.TAG_MESSAGE
import com.example.messagingm3.utils.Tag.TAG_MESSAGES
import com.example.messagingm3.utils.groupMessageByDate
import org.junit.Rule
import org.junit.Test

class MessagesTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Empty_Messages_Displayed() {
        composeTestRule.setContent {
            Messages(
                messages = emptyList(),
                onMessageSelected = {},
                unsendMessage = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_EMPTY).assertIsDisplayed()
    }

    @Test
    fun assert_Empty_Messages_Never_Displayed() {
        composeTestRule.setContent {
            Messages(
                messages = MessageFactory.makeMessages(),
                onMessageSelected = {},
                unsendMessage = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_EMPTY).assertDoesNotExist()
    }

    @Test
    fun assert_Messages_Displayed() {
        val messages = MessageFactory.makeMessages()

        composeTestRule.setContent {
            Messages(
                messages = messages,
                onMessageSelected = {},
                unsendMessage = {}
            )
        }

        groupMessageByDate(messages = messages)?.forEach { entry ->
            composeTestRule.onNodeWithTag(TAG_MESSAGES)
                .onChildren()
                .assertAny(hasTestTag(entry.key.timeInMillis.toString()))

            entry.value.forEach { message ->
                composeTestRule.onNodeWithTag(TAG_MESSAGES)
                    .onChildren()
                    .assertAny(hasTestTag(TAG_MESSAGE + message.id))
            }
        }
    }
}