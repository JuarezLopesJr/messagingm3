package com.example.messagingm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.platform.app.InstrumentationRegistry
import com.example.messagingm3.ui.screens.EmptyConversation
import org.junit.Rule
import org.junit.Test

class EmptyConversationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Empty_Message_Displayed() {
        composeTestRule.setContent {
            EmptyConversation()
        }

        composeTestRule.onNodeWithText(
            getTestString(R.string.label_no_messages)
        ).assertIsDisplayed()
    }
}