package com.example.messagingm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.messagingm3.ui.screens.MessageActions
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class MessageActionsTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Unsend_Action_Displayed() {
        composeTestRule.setContent {
            MessageActions(onDismiss = {}, onUnsendMessage = {})
        }

        composeTestRule
            .onNodeWithText(getTestString(R.string.action_unsend_message))
            .assertIsDisplayed()
    }

    @Test
    fun assert_Unsend_Action_Triggers_Callback() {
        val unsendListener: () -> Unit = mock()

        composeTestRule.setContent {
            MessageActions(onDismiss = {}, onUnsendMessage = unsendListener)
        }

        composeTestRule
            .onNodeWithText(getTestString(R.string.action_unsend_message))
            .performClick()

        verify(unsendListener).invoke()
    }
}