package com.example.messagingm3.viewmodel

import androidx.lifecycle.ViewModel
import com.example.messagingm3.data.ConversationEvent
import com.example.messagingm3.data.ConversationState
import com.example.messagingm3.data.Message
import com.example.messagingm3.data.MessageDirection
import java.util.Calendar
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class ConversationViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(ConversationState())

    val uiState = _uiState.asStateFlow()

    /* hardcoded SENT because it'll always be sent, 'me' because it'll always be me
      in this mocked Message constructor */
    private fun buildMessage(message: String): Message {
        return Message(
            id = _uiState.value.messages.size.toString(),
            direction = MessageDirection.SENT,
            dateTime = Calendar.getInstance(),
            sender = "me",
            message = message
        )
    }

    private fun excludeMessage(id: String): List<Message> {
        return _uiState.value.messages.toMutableList().apply {
            removeAt(
                _uiState.value.messages.indexOfFirst {
                    it.id == id
                }
            )
        }.toList()
    }

    fun handleEvent(event: ConversationEvent) {
        when (event) {
            /* mocking real scenario where the message would be sent to an API, first convert
             the list to a MutableList, add the result of buildMessage then convert back to List */
            is ConversationEvent.SendMessage -> {
                _uiState.value = _uiState.value.copy(
                    messages = _uiState.value.messages.toMutableList().apply {
                        add(buildMessage(event.message))
                    }.toList()
                )
            }

            is ConversationEvent.UnsendMessage -> {
                _uiState.value = _uiState.value.copy(
                    messages = excludeMessage(event.id),
                    selectedMessage = null
                )
            }

            is ConversationEvent.SelectMessage -> {
                _uiState.value = _uiState.value.copy(
                    selectedMessage = _uiState.value.messages.find {
                        it.id == event.id
                    }
                )
            }

            ConversationEvent.UnselectMessage -> {
                _uiState.value = _uiState.value.copy(
                    selectedMessage = null
                )
            }
        }
    }
}