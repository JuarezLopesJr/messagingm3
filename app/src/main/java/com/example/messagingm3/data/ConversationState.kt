package com.example.messagingm3.data

import com.example.messagingm3.utils.ContactFactory
import com.example.messagingm3.utils.MessageFactory

data class ConversationState(
    val messages: List<Message> = MessageFactory.makeMessages(),
    val contacts: List<Contact> = ContactFactory.makeContacts(),
    val selectedMessage: Message? = null
)