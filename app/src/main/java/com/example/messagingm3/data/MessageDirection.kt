package com.example.messagingm3.data

enum class MessageDirection {
    RECEIVED, SENT
}