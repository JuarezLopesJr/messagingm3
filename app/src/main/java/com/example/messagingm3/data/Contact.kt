package com.example.messagingm3.data

data class Contact(val name: String)