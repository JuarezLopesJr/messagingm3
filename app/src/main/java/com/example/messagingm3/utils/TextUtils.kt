package com.example.messagingm3.utils

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.withStyle
import com.example.messagingm3.data.Contact

/* function to replace the '@' when triggering the mention to contact */
fun stripMentionSymbol(text: String?) = text?.replace("@", "") ?: ""

/* getting the current selected word in the TextField, to be able to get only the mention
  to contacts that use '@'. TextFieldValue contains the current value of the input */
fun selectedWord(textState: TextFieldValue): String? {
    val startSelection = textState.selection.start
    var textPosition = 0

    for (currentWord in textState.text.split(" ")) {
        textPosition += currentWord.length + 1
        if (textPosition >= startSelection) return currentWord
    }

    return null
}

/* check condition to show contact list bar */
fun inputShouldTriggerSuggestions(
    contacts: List<Contact>,
    query: String?
) = query != null && query.startsWith("@") && !contacts.any {
    it.name == stripMentionSymbol(query)
}

/* autocompleting the contact selection inside the input text field */
fun TextFieldValue.replaceText(
    start: Int,
    end: Int,
    replaceWith: String
): TextFieldValue {
    val newText = this.text.replaceRange(
        startIndex = start,
        endIndex = end,
        replacement = replaceWith
    )

    val endOfNewWord = start + replaceWith.length

    val range = TextRange(start = endOfNewWord, end = endOfNewWord)

    return this.copy(text = newText, selection = range)
}

/* highlighting string with '@' mention */
fun buildAnnotatedStringWithColor(
    text: String,
    color: Color
): AnnotatedString {
    val words = text.split(" ")
    val builder = AnnotatedString.Builder()

    words.forEachIndexed { index, word ->
        if (word.startsWith("@")) {
            builder.withStyle(
                style = SpanStyle(color = color)
            ) {
                append(word)
            }
        } else {
            builder.append(word)
        }

        if (index < words.size - 1) builder.append(" ")
    }
    return builder.toAnnotatedString()
}

/* this class is necessary to override the current input style inside the TextField composable */
class MentionHighlightTransformation(private val color: Color) : VisualTransformation {
    override fun filter(text: AnnotatedString): TransformedText {
        return TransformedText(
            buildAnnotatedStringWithColor(text.toString(), color),
            OffsetMapping.Identity
        )
    }
}