package com.example.messagingm3.utils

import com.example.messagingm3.R
import com.example.messagingm3.data.Contact
import com.example.messagingm3.data.Message
import com.example.messagingm3.data.MessageDirection
import java.util.Calendar

object MessageFactory {
    fun makeMessages(): List<Message> {
        return listOf(
            Message(
                "0",
                MessageDirection.SENT,
                Calendar.getInstance().also {
                    it.add(Calendar.DAY_OF_YEAR, -5)
                    it.set(Calendar.HOUR_OF_DAY, 5)
                },
                "Mussum",
                "Cacildis"
            ),
            Message(
                "1",
                MessageDirection.RECEIVED,
                Calendar.getInstance().also {
                    it.add(Calendar.DAY_OF_YEAR, -5)
                    it.set(Calendar.HOUR_OF_DAY, 5)
                },
                "Mussum",
                "Cachacis"
            ),
            Message(
                "2",
                MessageDirection.RECEIVED,
                Calendar.getInstance().also {
                    it.add(Calendar.DAY_OF_YEAR, -4)
                    it.set(Calendar.HOUR_OF_DAY, 4)
                },
                "Mussum",
                "Cade o mezis ? \uD83D\uDE0A"
            ),
            Message(
                "3",
                MessageDirection.SENT,
                Calendar.getInstance().also {
                    it.add(Calendar.DAY_OF_YEAR, -2)
                },
                "Mussum",
                "Ta no alambiquis"
            ),
            Message(
                "4",
                MessageDirection.SENT,
                Calendar.getInstance().also {
                    it.add(Calendar.DAY_OF_YEAR, -2)
                },
                "Mussum",
                image = R.drawable.ic_launcher_foreground
            ),
            Message(
                "5",
                MessageDirection.SENT,
                Calendar.getInstance(),
                "Chapolin",
                image = R.drawable.ic_launcher_background
            ),
            Message(
                "6",
                MessageDirection.SENT,
                Calendar.getInstance(),
                "Tiao Macale",
                image = R.drawable.ic_launcher_foreground
            )
        )
    }
}

object ContactFactory {
    fun makeContacts(): List<Contact> {
        return listOf(
            Contact("Zacaria"),
            Contact("Tiao Macale"),
            Contact("Dede"),
            Contact("Chapolin"),
            Contact("Seu Madruga"),
            Contact("Chaves"),
        )
    }
}

/* configuring to group based only on the day, month and year. This is to avoid multiples
   groups by hour, minute and seconds */
fun groupMessageByDate(messages: List<Message>?): Map<Calendar, List<Message>>? {
    return messages?.let {
        /* sort to correct display items in the reversed layout */
        it.sortedByDescending { order ->
            order.dateTime.timeInMillis
        }.groupBy { message ->
            message.dateTime.apply {
                set(Calendar.HOUR_OF_DAY, 0)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
                set(Calendar.MILLISECOND, 0)
            }
        }
    }
}

object Tag {
    const val TAG_HEADER = "top_bar_header"
    const val TAG_MESSAGES = "messages"
    const val TAG_MESSAGE = "message_"
    const val TAG_INPUT_BAR = "input_bar"
    const val TAG_MESSAGE_ACTIONS = "message_actions"
    const val TAG_EMPTY = "empty"
    const val TAG_TEXT = "text"
    const val TAG_IMAGE = "image"
    const val TAG_MESSAGE_INPUT = "message_input"
    const val TAG_SEND_MESSAGE = "send_message"
    const val TAG_MENTIONS = "mentions"
    const val TAG_CONTACTS = "contacts"
}