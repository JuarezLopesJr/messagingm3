package com.example.messagingm3.utils

import androidx.annotation.DrawableRes
import com.example.messagingm3.data.Message
import com.example.messagingm3.data.MessageDirection
import java.util.Calendar
import java.util.UUID

object TestDataFactory {
    fun randomString() = UUID.randomUUID().toString()

    fun makeMessage(
        text: String = "",
        @DrawableRes image: Int? = null
    ): Message {
        return Message(
            id = randomString(),
            direction = MessageDirection.SENT,
            dateTime = Calendar.getInstance().also {
                it.add(Calendar.DAY_OF_YEAR, -5)
            },
            sender = randomString(),
            message = text,
            image = image
        )
    }
}