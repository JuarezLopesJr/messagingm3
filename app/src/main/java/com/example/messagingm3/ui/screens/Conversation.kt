@file:OptIn(
    ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class
)

package com.example.messagingm3.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import com.example.messagingm3.R
import com.example.messagingm3.data.ConversationEvent
import com.example.messagingm3.data.ConversationState
import com.example.messagingm3.utils.Tag.TAG_HEADER

@Composable
fun Conversation(
    modifier: Modifier = Modifier,
    state: ConversationState,
    handleEvent: (ConversationEvent) -> Unit
) {
    Column(modifier = modifier) {
        Header(onClose = {})
        Messages(
            modifier = Modifier.weight(1f),
            messages = state.messages,
            onMessageSelected = {
                handleEvent(ConversationEvent.SelectMessage(it))
            },
            unsendMessage = {
                handleEvent(ConversationEvent.UnsendMessage(it))
            }
        )
        InputBar(
            sendMessage = {
                handleEvent(ConversationEvent.SendMessage(it))
            },
            contacts = state.contacts
        )
    }

    if (state.selectedMessage != null) {
        MessageActions(
            onDismiss = { handleEvent(ConversationEvent.UnselectMessage) },
            onUnsendMessage = {
                handleEvent(ConversationEvent.UnsendMessage(state.selectedMessage.id))
            }
        )
    }
}

@Composable
fun Header(
    modifier: Modifier = Modifier,
    onClose: () -> Unit
) {
    TopAppBar(
        modifier = modifier.testTag(TAG_HEADER),
        title = {
            Text(
                text = stringResource(id = R.string.title_chat),
                fontSize = 18.sp
            )
        },
        navigationIcon = {
            IconButton(onClick = onClose) {
                Icon(
                    imageVector = Icons.Default.Close,
                    contentDescription = stringResource(id = R.string.cd_close_conversation)
                )
            }
        }
    )
}