@file:OptIn(ExperimentalLifecycleComposeApi::class)

package com.example.messagingm3.ui.screens

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.messagingm3.viewmodel.ConversationViewModel

@Composable
fun MessagingScreen(modifier: Modifier = Modifier) {
    val viewModel = viewModel<ConversationViewModel>()
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    Conversation(
        modifier = modifier.fillMaxSize(),
        state = state,
        handleEvent = viewModel::handleEvent
    )
}