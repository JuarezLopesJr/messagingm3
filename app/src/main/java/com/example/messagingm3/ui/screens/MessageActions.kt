package com.example.messagingm3.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.example.messagingm3.R
import com.example.messagingm3.utils.Tag.TAG_MESSAGE_ACTIONS

@Composable
fun MessageActions(
    modifier: Modifier = Modifier,
    onDismiss: () -> Unit,
    onUnsendMessage: () -> Unit,
) {
    Dialog(onDismissRequest = onDismiss) {
        Box(
            modifier = modifier
                .testTag(TAG_MESSAGE_ACTIONS)
                .sizeIn(minWidth = 280.dp, minHeight = 80.dp)
                .background(MaterialTheme.colorScheme.surface),
            contentAlignment = Alignment.Center
        ) {
            TextButton(onClick = onUnsendMessage) {
                Text(text = stringResource(id = R.string.action_unsend_message))
            }
        }
    }
}