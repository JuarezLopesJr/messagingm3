package com.example.messagingm3.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.messagingm3.R
import com.example.messagingm3.data.Contact
import com.example.messagingm3.utils.Tag.TAG_CONTACTS
import com.example.messagingm3.utils.stripMentionSymbol

@Composable
fun Mentions(
    modifier: Modifier = Modifier,
    contacts: List<Contact>,
    query: String?,
    onMentionClicked: (query: String, mention: String) -> Unit
) {
    val mentions = contacts.filter {
        val withoutMentionSymbol = stripMentionSymbol(query?.lowercase())

        it.name.lowercase().startsWith(withoutMentionSymbol)
    }

    Column(modifier = modifier) {
        Divider()

        LazyRow(modifier = Modifier.testTag(TAG_CONTACTS)) {
            items(mentions) { contact ->
                Text(
                    modifier = Modifier
                        .clickable(
                            onClickLabel = stringResource(
                                id = R.string.cd_mention_contact,
                                contact.name
                            )
                        ) {
                            onMentionClicked(query!!, "@${contact.name}")
                        }
                        .padding(16.dp),
                    text = contact.name
                )
            }
        }
    }
}