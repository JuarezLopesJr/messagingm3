@file:OptIn(
    ExperimentalFoundationApi::class, ExperimentalFoundationApi::class,
    ExperimentalFoundationApi::class, ExperimentalFoundationApi::class,
    ExperimentalFoundationApi::class
)

package com.example.messagingm3.ui.screens

import android.annotation.SuppressLint
import android.text.format.DateUtils.isToday
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.CustomAccessibilityAction
import androidx.compose.ui.semantics.customActions
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.text
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.messagingm3.R
import com.example.messagingm3.data.Message
import com.example.messagingm3.data.MessageDirection
import com.example.messagingm3.utils.Tag.TAG_EMPTY
import com.example.messagingm3.utils.Tag.TAG_IMAGE
import com.example.messagingm3.utils.Tag.TAG_MESSAGE
import com.example.messagingm3.utils.Tag.TAG_MESSAGES
import com.example.messagingm3.utils.Tag.TAG_TEXT
import com.example.messagingm3.utils.buildAnnotatedStringWithColor
import com.example.messagingm3.utils.groupMessageByDate
import java.text.SimpleDateFormat
import java.util.Calendar

@Composable
fun Messages(
    modifier: Modifier = Modifier,
    messages: List<Message>? = null,
    onMessageSelected: (String) -> Unit,
    unsendMessage: (String) -> Unit
) {
    val groupedMessage = groupMessageByDate(messages)
    val unsendMessageLabel = stringResource(id = R.string.action_unsend_message)
    val scrollState = rememberLazyListState()

    /* triggering the auto-scroll when the message list change. scrollToItem(0) because the list
      is reversed, so the first item is the last entered item */
    LaunchedEffect(messages) {
        scrollState.scrollToItem(0)
    }

    if (messages.isNullOrEmpty()) {
        EmptyConversation(modifier = modifier.fillMaxSize())
    } else {
        /* reversing the layout to auto-scroll to the last message sent, without the need to
         calculate the height offset after the scroll has taken place */
        LazyColumn(
            modifier = modifier.testTag(TAG_MESSAGES),
            state = scrollState,
            reverseLayout = true,
            contentPadding = PaddingValues(vertical = 8.dp, horizontal = 16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            groupedMessage?.onEach { entry ->

                items(entry.value) { message ->
                    MessageItem(
                        modifier = Modifier
                            .testTag(TAG_MESSAGE + message.id)
                            .semantics {
                                customActions = listOf(
                                    CustomAccessibilityAction(unsendMessageLabel) {
                                        unsendMessage(message.id)
                                        true
                                    }
                                )
                            }
                            .fillMaxWidth(),
                        message = message,
                        onLongPress = onMessageSelected
                    )
                }
                /* changing position of the sticky header to correct the placement inside
                   reversed layout */
                stickyHeader {
                    MessageHeader(
                        modifier = Modifier.testTag(entry.key.timeInMillis.toString()),
                        isToday = isToday(entry.key.time.time),
                        date = entry.key
                    )
                }
            }
        }
    }
}

@SuppressLint("SimpleDateFormat")
@Composable
fun MessageItem(
    modifier: Modifier = Modifier,
    message: Message,
    onLongPress: (String) -> Unit
) {
    val dateFormat = rememberSaveable {
        SimpleDateFormat("hh:mm")
    }

    var displayTime by rememberSaveable { mutableStateOf(false) }

    val formattedSentTime = dateFormat.format(message.dateTime.time)

    val messageWithSentTime = stringResource(
        id = R.string.message_with_time,
        formattedSentTime,
        message.message
    )

    val alignment = if (message.direction == MessageDirection.SENT)
        Alignment.End else Alignment.Start

    Column(modifier = modifier) {
        Box(
            modifier = Modifier
                .semantics {
                    text = AnnotatedString(messageWithSentTime)
                }
                .pointerInput(Unit) {
                    detectTapGestures(
                        onTap = { displayTime = !displayTime },
                        onLongPress = {
                            /* user can only delete sent messages */
                            if (message.direction == MessageDirection.SENT) {
                                onLongPress(message.id)
                            }
                        }
                    )
                }
                .align(alignment = alignment)
                .background(
                    MaterialTheme.colorScheme.primary,
                    RoundedCornerShape(6.dp)
                )
                .padding(8.dp)
        ) {
            MessageBody(
                modifier = Modifier.align(Alignment.Center),
                message = message
            )
        }

        AnimatedVisibility(
            modifier = Modifier.align(alignment = alignment),
            visible = displayTime
        ) {
            Text(
                modifier = Modifier
                    .padding(top = 4.dp),
                text = formattedSentTime,
                fontSize = 12.sp
            )
        }
    }
}

@SuppressLint("SimpleDateFormat")
@Composable
fun MessageHeader(
    modifier: Modifier = Modifier,
    isToday: Boolean,
    date: Calendar
) {
    val label = if (isToday) {
        stringResource(id = R.string.label_today)
    } else {
        val dateFormat = remember {
            SimpleDateFormat("dd MMM yyyy")
        }
        dateFormat.format(date.time)
    }

    Box(
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            modifier = modifier
                .fillMaxWidth(0.3f)
                .background(
                    MaterialTheme.colorScheme.onSurface.copy(alpha = 0.05f),
                    RoundedCornerShape(6.dp)
                )
                .padding(vertical = 4.dp),
            text = label,
            fontSize = 14.sp,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
private fun MessageBody(
    modifier: Modifier = Modifier,
    message: Message
) {
    when {
        message.message.isNotEmpty() -> {
            Text(
                modifier = modifier.testTag(TAG_TEXT),
                text = buildAnnotatedStringWithColor(
                    text = message.message,
                    color = Color.Blue
                ),
                color = MaterialTheme.colorScheme.onPrimary
            )
        }

        message.image != null -> {
            Image(
                modifier = Modifier
                    .size(120.dp)
                    .testTag(TAG_IMAGE),
                painter = painterResource(id = message.image),
                contentDescription = message.altText
            )
        }
        /* message.image != null -> {
             Image(
                 modifier = Modifier.size(120.dp),
                 bitmap = BitmapFactory.decodeResource(
                     LocalContext.current.resources, message.image
                 ).asImageBitmap(),
                 contentDescription = message.altText
             )
         }*/
    }
}

@Composable
fun EmptyConversation(modifier: Modifier = Modifier) {
    Box(modifier = modifier.testTag(TAG_EMPTY)) {
        Text(text = stringResource(id = R.string.label_no_messages))
    }
}