@file:OptIn(
    ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class
)

package com.example.messagingm3.ui.screens

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import com.example.messagingm3.R
import com.example.messagingm3.data.Contact
import com.example.messagingm3.utils.MentionHighlightTransformation
import com.example.messagingm3.utils.Tag.TAG_INPUT_BAR
import com.example.messagingm3.utils.Tag.TAG_MENTIONS
import com.example.messagingm3.utils.Tag.TAG_MESSAGE_INPUT
import com.example.messagingm3.utils.Tag.TAG_SEND_MESSAGE
import com.example.messagingm3.utils.inputShouldTriggerSuggestions
import com.example.messagingm3.utils.replaceText
import com.example.messagingm3.utils.selectedWord

@SuppressLint("UnrememberedMutableState")
@Composable
fun InputBar(
    modifier: Modifier = Modifier,
    sendMessage: (String) -> Unit,
    contacts: List<Contact>
) {
    val (textValue, setTextValue) = remember {
        mutableStateOf(TextFieldValue())
    }

    val showMentions by derivedStateOf {
        textValue.text.isNotEmpty() &&
                inputShouldTriggerSuggestions(
                    contacts = contacts,
                    query = selectedWord(textState = textValue)
                )
    }

    val iconColor = if (textValue.text.isNotEmpty()) {
        MaterialTheme.colorScheme.primary
    } else {
        MaterialTheme.colorScheme.onSurface.copy(alpha = 0.4f)
    }

    Column(modifier = modifier.testTag(TAG_INPUT_BAR)) {

        AnimatedVisibility(visible = showMentions) {
            Mentions(
                modifier = Modifier
                    .testTag(TAG_MENTIONS)
                    .fillMaxWidth(),
                contacts = contacts,
                query = selectedWord(textValue),
                onMentionClicked = { query, result ->
                    val startIndex = textValue.text.indexOf(query)

                    setTextValue(
                        textValue.replaceText(
                            start = startIndex,
                            end = startIndex + query.length,
                            replaceWith = result
                        )
                    )
                }
            )
        }

        Divider()

        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .testTag(TAG_MESSAGE_INPUT),
            value = textValue,
            onValueChange = setTextValue,
            placeholder = {
                Text(
                    text = stringResource(id = R.string.hint_send_message),
                    color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.4f)
                )
            },
            colors = TextFieldDefaults.textFieldColors(
                containerColor = MaterialTheme.colorScheme.surface
            ),
            trailingIcon = {
                IconButton(
                    modifier = Modifier.testTag(TAG_SEND_MESSAGE),
                    onClick = {
                        sendMessage(textValue.text)
                        setTextValue(TextFieldValue(""))
                    },
                    enabled = textValue.text.isNotEmpty()
                ) {
                    Icon(
                        imageVector = Icons.Default.Send,
                        tint = iconColor,
                        contentDescription = stringResource(id = R.string.cd_send_message)
                    )
                }
            },
            visualTransformation = MentionHighlightTransformation(Color.Blue)
        )
    }
}